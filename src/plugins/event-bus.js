const eventBus = {}

eventBus.install = function (Vue) {
    Vue.prototype.$bus = new Vue()
}
//Esto es un bus de eventos. Se usa en vez de vuex 
//cuando el proyecto es chiquito 

export default eventBus