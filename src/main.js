import Vue from 'vue'
import App from '@/App.vue'
import EventBus from '@/plugins/event-bus'
import msToMm from '@/filters/ms-to-mm'
import blur from '@/directives/blur'
import router from '@/router'
import store from '@/store'
import i18n from '@/i18n'

Vue.config.productionTip = false;
Vue.use(blur)
Vue.use(msToMm)
Vue.use(EventBus)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
