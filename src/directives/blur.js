const blur = {} //Las directivas son estas madres 
// que usas en el html como v-if v-for y asi
//y puedes hacer las tuyas custom como aqui 

function setBlur(el, binding){ //js puro
    el.style.filter = !binding.value ? 'blur(3px)' : 'none'
    el.style.cursor = !binding.value ? 'not-allowed' : 'inherit' 

    el.querySelectorAll('button').forEach(a => {
        if (!binding.value)
            a.setAttribute('disabled', true)
        else    
            a.removeAttribute('disabled')
    })
}

blur.install = function (Vue) {
    Vue.directive('blur', {
        bind (el, binding){
            setBlur(el, binding)
        }
    })
}

export default blur