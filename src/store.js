import Vue from 'vue'
import Vuex from 'vuex'
import trackService from '@/services/track'

Vue.use(Vuex);

//Este es el store de Vuex 
//Puedes importar los getters, mutations y actions con 
// ...mapState o ...mapGetters y asi
// y es como si fueran metodos de tu componente .Vue 
export default new Vuex.Store({
    state: {
       track: {}
    },
    getters: {
        trackTitle(state, getters){
            if (!state.track.name) 
                return ''
            return `${state.track.name} - ${state.track.artists[0].name}`
        }
    },
    mutations: {
        setTrack(state, track) {
            state.track = track;
        }
    },
    actions: {
        getTrackById(context, payload) {
            return trackService.getById(payload.id)
                .then(res => {
                    //context == store 
                    //res == track
                    context.commit('setTrack', res) 
                    return res
                })
        }
    }
})
