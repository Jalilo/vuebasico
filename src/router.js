import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/components/Search'
import About from '@/components/About'
import TrackDetail from '@/components/TrackDetail'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            component: Search,
            name: 'search'
        },
        {
            path: '/about',
            component: About,
            name: 'about'
        },
        {
            path: '/track/:id',
            component: TrackDetail,
            name: 'track'
        },

    ],
    mode: 'history'
})