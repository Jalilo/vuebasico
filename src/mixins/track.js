const trackMixin = {
    //Aqui puedes poner como un objeto completo de Vue e importarlo en todas
    //las clases que usen los mismos metodos con mixins: []
    methods: {
        selectTrack(){
            if (!this.track.preview_url)
              return
            this.$store.commit('setTrack', this.track)
        },
    }
}

export default trackMixin